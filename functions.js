const factorialIterativo = (number)=>{
    let res = 1;
    for(let i = number; i>=1; i--){
        res = res * i;
    }
    return res;
};

const factorialRecursivo = (number)=>{
    if(number<=1) return 1;
    return number * factorialRecursivo(number - 1);
};



const cleanWord = (word)=>{
    let regExpr = /[^a-zA-Z]/g;
    return word.replace(regExpr,'');
};


const countWord = (str)=>{
    let cad = str.split(' ');
    let words = cad.map((item)=>{return cleanWord(item);});

    let len = words[0].length;
    let index = 0;
    words.map((item,key)=>{
        let lenNow = item.length;

        if(item != ""){
            if(len > lenNow){
                len = len;
            }
            if(len < lenNow){
                len = lenNow;
                index = key;
            }
            if(lenNow === len){
                len = len;
            }
        }

    });
    return words[index];
};

/*document.getElementById('input').addEventListener('input',(event)=>{
    let option = document.getElementById('options').value;
    let param = event.target.value;
});*/


document.getElementById('form_functions').addEventListener('submit',(event)=>{
    event.preventDefault();
    let param = document.getElementById('input').value;
    let option = document.getElementById('options').value;

    let result = '';

    if(option == 0){
        param = Number.parseInt(param);
        if(Number.isInteger(param)){
            result = 'El factorial con function iterativa es : ' + factorialIterativo(param)
            console.log(result);
        }else{
            alert("La entrada debe de ser un numero")
        }
        
    }
    if(option == 1){
        param = Number.parseInt(param);
        if(Number.isInteger(param)){
            result = 'El factorial con function recursiva es : ' + factorialRecursivo(param)
            console.log(result);
        }else{
            alert("La entrada debe de ser un numero");
        }
    }
    if(option == 2){
        result = 'La palabra con mayor longitud es : ' + countWord(param);
        console.log(result);
    }

    document.getElementById('result').innerHTML=result;


});
